# Opscale - Prueba técnica - Desarrollador Laravel

## Configuración del Proyecto Laravel con Laravel Nova

Este repositorio contiene un proyecto Laravel integrado con Laravel Nova. A continuación se detallan los pasos esenciales para configurar y ejecutar el proyecto localmente.

### Requisitos Previos

- PHP `8.1` - `8.3`
- Tener instaladas y configuradas las extensiones de php necesarias para el funcionamiento del proyecto de laravel, conexion a la base de datos, etc...
- Composer instalado globalmente
- Redis (opcional, para colas)

# Pasos de Configuración

## Clonar el Repositorio

Clona este repositorio en tu máquina local:

```bash
git clone https://gitlab.com/checho/opscale-test.git
```
```bash
cd opscale-test/
```

## Instalar Dependencias

Instala las dependencias de PHP:

```bash
composer install
```

`Nota:` Al ejecutar `composer install`, se le pedirá que introduzca un nombre de usuario y una contraseña. Debe utilizar el correo electrónico de su sitio web Nova para el nombre de usuario `(nelson.g.chicas.g@gmail.com)` y una clave de licencia se debe utilizar como contraseña `(CfA1k8vA1rIFxgPEPAjehv4vvBfFqMHQU8YZLsDrDth9c3lLDy)`. Estas credenciales autenticarán su sesión Composer como tener permiso para descargar el código fuente de Nova

## Configuración del Entorno

Chequea el archivo `.env` ya que tiene unos comentarios para la configuración adecuada de algunas variables tales como:

- `APP_URL`: Configura la URL base y el puerto para navegar en las rutas del proyecto.
- `QUEUE_CONNECTION`: opcionalmente, configura Redis para las colas y variables relacionadas.

## En caso de tener redis habilitado

- revisar la linea #17 y #18 del archivo .env
- descomentar la linea #77 del archivo app\Models\User.php
- comentar la linea #78 del archivo app\Models\User.php
- entrar en la ruta base del proyecto en una terminal independiente y ejecutar el comando `php artisan queue:work` para que escuche el job de los webook cuando se creado un usuario.

## Ejecutar Migraciones y Semillas (opcional)

`Nota`: la base de datos incluida en el repositorio ya incluye datos de prueba.

Ejecuta las migraciones para crear las tablas de la base de datos:

```bash
php artisan migrate:fresh
php artisan db:seed --force
```

Esto también ejecutará las semillas para crear un usuario predeterminado para iniciar sesión en Nova, el cual es: `principal_user@test.test` con contraseña: `supersecret`.

## Acceder a Laravel Nova

Si no has configurado un host personalizado (virtual host, ver `.env`), ejecuta el servidor de desarrollo de Laravel:

```bash
php artisan serve --host=localhost
```

Accede a tu aplicación en [http://localhost:8000](http://localhost:8000).


Para acceder al panel de administración de Nova, visita:

[http://localhost:8000/nova](http://localhost:8000/nova)

email: `principal_user@test.test`
contraseña: `supersecret`.

#

En el proyecto se encuentran incluidos 4 de los 6 retos de la prueba:

- Implementar Spatie Webhook Server y un recurso de Nova para que pueda crear un registro de webhook definiendo: método, url y encabezados. Cada vez que un usuario sea creado se debe enviar el payload del usuario a todos los webhooks creados.

- Crear un calendario usando FullCalendar utilizando la tabla de users, created_at para la fecha en calendario y el nombre de usuario como label.

- Crear una Nova Action (Como parte del Nova Tool) que use Fast Excel para importar y exportar todos los registros de usuarios, eligiendo el mapeo de los campos como parte del action.

- Crea una conexión con la API de OpenAI usando Laravel GPT para que en una conversación de ChatGPT detecte una función que devuelva todos los usuarios que han sido creado en las últimas 24 horas.

`Nota:` también intente incluir el reto numero 3 del listado de la prueba pero tuve inconvenientes para completarlo (vue-onboarding) se puede ver el codigo en la rama `feature/nova-tool-three`.

