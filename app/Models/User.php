<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'created_at',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => Carbon::parse($value)->format('Y-m-d H:i:s'),
        );
    }

    protected function emailVerifiedAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => filled($value) ? Carbon::parse($value)->format('Y-m-d H:i:s') : null,
        );
    }

    protected static function booted()
    {
        static::created(function ($user) {
            $webhooks = \App\Models\Webhook::all();

            foreach ($webhooks as $webhook) {
                \Spatie\WebhookServer\WebhookCall::create()
                    ->doNotSign()
                    ->url($webhook->url)
                    ->useHttpVerb($webhook->method)
                    ->withHeaders(is_array($webhook->headers) ? $webhook->headers : (array) $webhook->headers)
                    ->payload(['user' => $user])
                    // ->dispatch();
                    ->dispatchSync();
            }
        });
    }
}
