<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Webhook extends Model
{
    use HasFactory;

    protected $casts = [
        'headers' => 'array',
    ];

    protected $fillable = [
        'method', 'url', 'headers',
    ];

    public function setHeadersAttribute($value)
    {
        $this->attributes['headers'] = json_encode($value);
    }
}
