<?php

namespace App\Http\Controllers;

use App\GPT\Chats\GetRecentUsers\GetRecentUsersGPTChat;
use Illuminate\Http\Request;

class ChatGPTController extends Controller
{
    public function handleMessage(Request $request)
    {
        $message = $request->input('message');

        $chat = GetRecentUsersGPTChat::make();
        $chat->addMessage($message);
        $chat->send();

        return response()->json($chat->latestMessage()->content);
    }
}
