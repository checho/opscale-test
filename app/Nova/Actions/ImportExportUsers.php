<?php

namespace App\Nova\Actions;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Http\Requests\NovaRequest;
use Rap2hpoutre\FastExcel\FastExcel;
use Laravel\Nova\Actions\ActionResponse;

class ImportExportUsers extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $validator = Validator::make(request()->all(), [
            'action_type' => 'required|in:import,export',
            'field1' => 'required|in:name,email,created_at,email_verified_at|distinct',
            'field2' => 'required|in:name,email,created_at,email_verified_at|distinct|different:field1',
            'field3' => 'required|in:name,email,created_at,email_verified_at|distinct|different:field1,field2',
            'field4' => 'required|in:name,email,created_at,email_verified_at|distinct|different:field1,field2,field3',
            'file' => 'required_if:action_type,import|file|mimes:xlsx'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        if ($fields->action_type === 'export') {
            return $this->exportUsers($fields);
        } elseif ($fields->action_type === 'import') {
            return $this->importUsers($fields);
        }

        return Action::danger('Invalid action type selected.');
    }

    /**
     * Export users to an Excel file.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @return mixed
     */
    protected function exportUsers($fields)
    {
        $users = User::all();
        $mappedUsers = $users->map(function ($user) use ($fields) {
            return [
                $fields->field1 => $user->{$fields->field1},
                $fields->field2 => $user->{$fields->field2},
                $fields->field3 => $user->{$fields->field3},
                $fields->field4 => $user->{$fields->field4},
            ];
        });

        $fileName = 'users_export_' . now()->format('Y_m_d_H_i_s') . '.xlsx';

        (new FastExcel($mappedUsers))->export(storage_path('app/public/' . $fileName));

        return ActionResponse::download($fileName, asset('storage/' . $fileName));
    }

    /**
     * Import users from an Excel file.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @return mixed
     */
    protected function importUsers($fields)
    {
        $file = $fields->file->getRealPath();

        (new FastExcel)->import($file, function ($row) use ($fields) {
            User::insertOrIgnore([
                $fields->field1 => $row[$fields->field1],
                $fields->field2 => $row[$fields->field2],
                $fields->field3 => $row[$fields->field3],
                $fields->field4 => $row[$fields->field4],
                'password' => Hash::make('password')
            ]);
        });

        return Action::message('Users imported successfully.');
    }

    /**
     * Get the fields available on the action.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [];
    }
}
