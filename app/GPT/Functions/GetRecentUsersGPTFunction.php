<?php

namespace App\GPT\Functions;

use MalteKuhr\LaravelGPT\GPTFunction;
use App\Models\User;
use Closure;

class GetRecentUsersGPTFunction extends GPTFunction
{
    /**
     * Specifies a function to be invoked by the model. The function is implemented as a
     * Closure which may take parameters that are provided by the model. If extra arguments
     * are included in the documentation to optimize model's performance (by allowing it more
     * thinking time), these can be disregarded by not including them within the Closure
     * parameters.
     *
     * If the Closure returns null, the chat interaction is paused until the 'send()' method in
     * the request is invoked again. For all other return values, the response is JSON encoded
     * and forwarded to the model for further processing.
     *
     * @return Closure
     */
    public function function(): Closure
    {
        return function (): mixed {
            $recentUsers = User::where('created_at', '>=', now()->subDay())->where("created_at", "<=", now())->get();

            return [
                'users' => $recentUsers->map(fn ($user) => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'created_at' => $user->created_at,
                    'email_verified_at' => $user->email_verified_at ? $user->email_verified_at : null,
                ])
            ];
        };
    }

    /**
     * Defines the rules for input validation and JSON schema generation. Override this
     * method to provide custom validation rules for the function. The documentation will
     * have the same order as the rules are defined in this method.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'users' => 'array',
            'users.*.name' => 'required|string',
            'users.*.email' => 'required|email',
            'users.*.created_at' => 'required|date_format:Y-m-d H:i:s',
            'users.*.email_verified_at' => 'nullable|date_format:Y-m-d H:i:s',
        ];
    }

    /**
     * Describes the purpose and functionality of the GPT function. This is utilized
     * for generating the function documentation.
     *
     * @return string
     */
    public function description(): string
    {
        return 'Get all users created in the last 24 hours.';
    }
}