import Tool from './pages/Tool'

Nova.booting((app, store) => {
  Nova.inertia('ChatGptTool', Tool)
})

Nova.booting((Vue, store) => {
  Vue.component("ChatMessages", require("./components/Chat/ChatMessages.vue").default);
  Vue.component("MessageBox", require("./components/Chat/MessageBox.vue").default);
});

