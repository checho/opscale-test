import { ref } from 'vue'

export const useChat = () => {
    const messages = ref([])

    const getResponse = async (text) => {
        const resp = await Nova.request().post('/nova-vendor/chat-gpt-tool/message', {
            message: text
        })

        const data = resp

        return data
    }

    const onMessage = async (text) => {
        if (text.length === 0) return

        messages.value.push({
            id: new Date().getTime(),
            message: text,
            itsMine: true,
        })

        const {data} = await getResponse(text);

        messages.value.push({
            id: new Date().getTime(),
            message: data,
            itsMine: false,
        });
    }

    return {
        // Properties
        messages,

        // Methods
        onMessage,
    }
}
