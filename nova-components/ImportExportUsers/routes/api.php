<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Nova\Actions\ImportExportUsers;
use Laravel\Nova\Fields\ActionFields;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

// Route::get('/', function (Request $request) {
//     //
// });

Route::post('/run-import-export', function (Request $request) {
    $action = new ImportExportUsers();

    $fields = new ActionFields(collect($request->all()), collect([]));

    $users = User::all();

    return $action->handle($fields, $users);
});